const { gql } = require("apollo-server");
const { GraphQLScalarType, Kind } = require("graphql");

const typeDefs = gql`
  # Your schema will go here
  type Launch {
    id: ID!
    site: String
    mission: Mission
    rocket: Rocket
    isBooked: Boolean!
  }

  scalar Date

  type Rocket {
    id: ID!
    name: String
    type: String
  }

  type User {
    id: ID!
    email: String!
    trips: [Launch]!
    token: String
  }
  
  type Mission {
    name: String
    missionPatch(size: PatchSize): String
  }
  
  enum PatchSize {
    SMALL
    LARGE
  }

  type Query {
    launches: [Launch]!
    launch(id: ID!): Launch
    me: User
  }

  type Mutation {
    bookTrips(launchIds: [ID]!): TripUpdateResponse!
    cancelTrip(launchId: ID!): TripUpdateResponse!
    login(email: String): User
  }

  type TripUpdateResponse {
    success: Boolean!
    message: String
    launches: [Launch]
  }
`;

const dateScalar = new GraphQLScalarType({
  name: "Date",
  description: "Date custom scalar type",
  serialize: (value) => value.getTime(), // Convert outgoing Date to integer for JSON
  parseValue: (value) => new Date(value), // Convert incoming integer to Date
  parseLiteral: (ast) => {
    if (ast.kind == Kind.INT)
      return parseInt(ast.value, 10); // Convert hard-coded AST string to type expected by parseValue

    return null; // Invalid hard-coded value (not an integer)
  }
});

module.exports = {
  typeDefs,
  dateScalar
};
