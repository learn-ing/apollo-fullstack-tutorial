require('dotenv').config();
const { ApolloServer } = require("apollo-server");
const { typeDefs, dateScalar } = require("./schema");

const resolvers = {
  Date: dateScalar
};

const server = new ApolloServer({
  typeDefs,
  resolvers
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
});
